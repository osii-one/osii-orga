# Technology-specific Documentation Criteria for a Bill of Materials (BOM)

This TsDC only covers hardware BOMs.

## Mandatory Information

A BOM includes:

- _all_ components of the assembly to work as intended (this includes e.g. a casing)
- designation of the components as used in the rest of the documentation (e.g. in the assembly instructions or the PCB design)
- an unambiguous reference for each component:
	- standard components: _complete_ standard designation (e.g. ISO 4017 – M6 × 35 – 8.8)
	- non-standard components: approved manufacturer(s) + manufacturer part number
		- URLs may die, so **do not** substitute this with online shop links
	- self-designed components: reference to the respective folder/document within the documentation
	- other open source hardware: URL to the specific version (e.g. a release or commit)
- the quantity of each component as used in the assembly

## Best Practice

### Example Format

| ID                                                       | Item                                       | Quantity                              | Quantity-Sub                            | Reference                                                          | Make                                                                            | Comment                                                                                   | Manufacturer                                                                                                                     | MPN                                                  | ExDist                                      | ExDist PN                                   | ExDist Link                                       |
|----------------------------------------------------------|--------------------------------------------|---------------------------------------|-----------------------------------------|--------------------------------------------------------------------|---------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------|---------------------------------------------|---------------------------------------------|---------------------------------------------------|
| _that is used for the part throughout the documentation_ | _a human-readable description of the part_ | _quantity of the part OR subassembly_ | _quantity of the part in a subassembly_ | _path to design files OR standard designation OR URL to datasheet_ | _true/false (for easier sorting between manufactured and pruchased components)_ | _free form field, e.g. to explain Quantity (especially useful for screws; e.g. =6*part1)_ | _approved manufacturer for the part (or raw products to make the part); leave blank if not applicable (e.g. for standard parts)_ | _manufacturer part number for unambiguous reference_ | _optional: example distributor of the part_ | _optional: example distributor part number_ | _optional: link to part from example distributor_ |

See e.g. the [BOM of the 30cm Halbach Magnet](https://gitlab.com/osii/magnet/30cm-halbach-magnet/-/blob/main/src/BOM.csv) for a real-world example

A repo can also be structured with sub-assemblies, see the [proposed directory structure in the OSI² Contribution Guide](https://gitlab.com/osii/cab/-/blob/main/CONTRIBUTING.md?ref_type=heads#directory-structure).

### For File Formats

`CSV`:

- no special software required to read or process
  (in contrast to `XLSX` for instance),
  since it's plain text
- git-compatible (since it's plain text)
- suitable for further processing
  (other than e.g. markdown tables)

### For Information

- providing the full specification for non-standard components
  (e.g. via datasheets, potentially keeping a copy in the repository)
  helps finding alternative parts or manufacturers
  in case the defined ones are not available anymore
  or at the location of replication
- online shops links may die,
  but until then they're a good help when replicating the device
