# Technology-specific Documentation Criteria for a Software Bill of Materials (SBOM)

## Mandatory Information

An SBOM references the software needed to:

1. operate the hardware as intended,
2. modify the technical documentation (in a lossless manner),
3. replicate the fully functioning hardware following the assembly instructions.

**This includes:** software, firmware, libraries, dependencies, extensions, plugins etc.

**Exception:** software to process standard file formats or plain text.

An unambiguous reference suffices (e.g. URL to the specific release).

## Best Practice

### For Documentation

- licensing terms
- alternatives (e.g. in the case of [trapped software](
  https://www.gnu.org/philosophy/when-free-depends-on-nonfree.en.html))
