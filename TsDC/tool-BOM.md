# Technology-specific Documentation Criteria for a Bill of Materials for Special Tools (Tool-BOM)

## Mandatory Information

A Tool-BOM includes an unambiguous reference to all tools needed to

- handle non-standard parts of the assembly,
- carry out steps in the replication that are not obviously implied by the design
  (for the target group).

If a tool has to be manufactured,
the same TsDC as for self-designed components apply for it.
