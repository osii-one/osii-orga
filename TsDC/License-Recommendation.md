# License Recommendation for A4IM

To keep projects legally compatible within the OSI² community, we recommend the use of the following licenses:

| Type                | Detail                                                   | License                                                                                                                                         |
|---------------------|----------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------|
| Hardware            | technical documentation (CAD, calculations, instructions) | [CERN-OHL-W-2.0](https://ohwr.org/cern_ohl_w_v2.txt)                                                                                            |
| Software            | source code                                              | [Apache-2.0](https://www.apache.org/licenses/LICENSE-2.0.txt), [MIT](https://opensource.org/license/mit/) or any permissive open source license |
| QM-related material | e.g. MDR-related documents                               | [CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/legalcode)                                                                              |
| Media               | logos, group fotos, recordings of meetings etc.          | [CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/legalcode)                                                                              |
| Research            | publications, data etc.                                  | [CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/legalcode)                                                                              |
| Metadata            | e.g. of of deposited publications or hardware designs    | [CC0-1.0](https://creativecommons.org/publicdomain/zero/1.0/legalcode)                                                                          |

For background information please see [licensing.md](/CAB-Docs/licensing.md) in the `CAB-Docs`.

If you have any questions regarding this list or how to use the license,
please feel free to open an [issue](https://gitlab.com/osii/osii-cab/-/issues/new)
and assign [`@Moedn`](https://gitlab.com/Moedn).
