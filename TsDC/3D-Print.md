# Technology-specific Documentation Criteria for 3D Printed Mechanical Parts

## Mandatory Information

- geometry (provided by 3D models)
  - if necessary:
    - tolerances
    - surface specifications
- material properties
  - reference of material used for this part
  - can be a generic statement leastwise (e.g. PLA)
- printing-process
  - e.g. FDM, SLA, SLS, MJF, DMLS
- if necessary:
  - manufacturing specifications (e.g. special manufacturing process, machine settings)

## Best Practice

### For File Formats

- native source files in an open file format (e.g. [.FCStd](https://www.freecad.org/)),
  preferably git-compatible (e.g. [.SCAD](https://openscad.org/))
- export files in [.stp](https://en.wikipedia.org/wiki/ISO_10303)
  and [.stl](https://en.wikipedia.org/wiki/STL_(file_format))

### For Documentation

- rationale explaining considerations behind design decisions
- references to or copies of design requirements,
  e.g. to comply with regulations or technical standards

### For Production

to facilitate replication, please provide the following metadata:

- `infill` \[float\]
  - print parameter: infill (in %)
- `raft-brim` \[bool\]
  - 0 = design has no raft or brim
  - 1 = design includes raft or brim
- `supports` \[bool\]
  - 0 = design has no supports
  - 1 = design includes support(s)
- `resolution-mm` \[float\]
  - print parameter: resolution/layer height in mm
- `shell-thickness` \[float\]
  - shell thickness in mm
- `top-bottom-thickness` \[float\]
  - top/bottom thickness in mm
