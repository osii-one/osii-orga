---
title: Assessing Open Source IP
...

# Background

## General

In regards of intellectual property (IP),
the source code of software usually falls solely under copyright
(unless there's some weird software patent filed for it).

For hardware, multiple IP restriction schemes apply. Most notably:

- copyright
  (covering the technical documentation (or anything "creative", basically)),
- patent law
  (covering the functional part itself) and
- industrial design rights
  (covering visual designs that are purely utilitarian).

So to make hardware open source,
it must be "freed" in all applicable IP domains.

## Copyright

Copyright is granted to the creator of a work in the moment of creation –
and there is (as far as we know) no way to avoid it in the first place.
So the copyright holder is by default the only one allowed to use and replicate the work,
to modify and distribute it etc.

To solve this,
the copyright holder can grant anyone with a license that allows the discrimination- and royalty-free use,
modification and redistribution –
using an irrevocable open source license.

DIN SPEC 3105-1 states that for hardware being open source hardware (OSH)

> requires releasing the documentation under a license
> complying with the requirements of the OSWHA
> (Open Source Hardware Association) Definition 1.0

([source](https://gitlab.com/OSEGermany/OHS/-/blob/a222f452b46e716a9e833811ab63b46d9dcfaf25/DIN_SPEC_3105-1.md#34-four-rights-of-open-source-hardware))

Consequently, it's sufficient to just check the license under which the technical documentation has been published.
This is typically done by looking into the `LICENSE` file of the corresponding repository.

If it complies with the OSHWA Definition
(or FSF or OSI definition in the case of software),
it's deemed open source
(or rather, the material that falls under copyright is).

Unfortunately there is no central registry for OSHWA-compliant licenses
(or at least we couldn't find one).
However, we've created the [License List below](#license-list)
that should enable you to assess the licensing terms without being or hiring a lawyer.

## Patent Law

Regarding patent law, technical inventions are "born open",
meaning that an innovation needs to be actively restricted (by filing a patent)
in order to gain exclusive licensing rights (in contrast to copyright, see above).\
However, anyone, _not just the originator_, can file a patent –
so you want to make sure that no one patents an open source hardware project –
and thereby kills it.

There are generally two scenarios:

1. patent + hardware-specific open source license (also called "patentleft")
   → generally never the case
2. no patent.

If scenario 2 is given,
the project can prevent patenting using a method called "defensive publishing":
publicly release all relevant information about the functional part of the hardware,
therewith defeating the novelty criterion needed to file a patent.\
Generally, this is the method of choice for most open source hardware projects.

As we currently don't have the resources to conduct a worldwide patent research
on every hardware project that asks for free reviewing,
we instead ask for a respective declaration to be signed
(see [Assessment Procedure](IP-freedom-declaration.md) below).

If the technical documentation is deemed to be complete,
according to DIN SPEC 3105-1,
it automatically fulfills the conditions for defensive publishing as well.

## Industrial Design Rights

Similar to patent law,
it is possible to restrict the use of (2D or 3D) visual design of hardware –
the exclusive rights for the IP holder are much broader than for copyright
(which, for the originator, comes automatically and free of charge),
but must be filed (usually at the competent patent office).

As for patents, novelty can be defeated using defensive publishing.
And also here, we currently do not have the resources to conduct respective research –
and ask for a signed declaration instead.

# License List

## Recommended for OSH

- CERN OHL v2.0 variant
  (modern, well-maintained, hardware-specific open source license):
  - `CERN-OHL-S-2.0`: strong reciprocality
    (preserve licensing terms in all derivative works)
  - `CERN-OHL-W-2.0`: weak reciprocality
    (only direct changes must preserve licensing terms; adoption can be proprietary)
  - `CERN-OHL-P-2.0`: no reciprocality
    (no restrictions on licensing terms of derivatives or adoptions; they can even be proprietary)
- `CERN-OHL-1.2`
  (last CERN-OHL compatible with GPL)
- `SHL-2.1`: modern, well-maintained permissive license that was specifically designed
  "to add hardware wrappers to existing open source software licenses."
  ([source](https://www.oshwa.org/2020/04/04/new-cern-open-source-hardware-licenses-mark-a-major-step-forward/))

## Accepted for OSH

Generally any license that is endorsed either by OSI or FSF also works for hardware documentation –
it's just optimized for a different kind of material (e.g. software).\
So it might not be ideal
(e.g. leaving some legal questions open),
but in principle the material is open source.

- find the full and always up-to-date list here: <https://spdx.org/licenses/>\
  … here a selection of commonly used licenses (or any earlier versions of them are also ok):
  - `CC-BY-4.0`
  - `CC-BY-SA-4.0`
  - `CC0-1.0`
  - `GPL-3.0-or-later`
  - `MIT`
  - `Apache-2.0`
- We also accept
  - `TAPR-OHL-1.0`
  
The suspected license is not on the list?
Just file an issue in this repo and assign an admin,
e.g. [`@Moedn`](https://gitlab.com/Moedn).

## NOT-accepted for OSH

… any license that doesn't comply with the OSHWA definition;\
e.g. for obvious reasons:

- any non-commercial license such as `CC-BY-NC-4.0`
  - see reasoning [here](https://mifactori.de/non-commercial-is-not-open-source/)
- any non-derivative license such as `CC-BY-ND-4.0`
  - see reasoning [here](https://opensource.stackexchange.com/a/358)
