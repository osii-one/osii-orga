# Review of Software

## Note to Reviewers

- every criterion has to be checked by at least 2 reviewers; reviewers shall specify, which points they have checked
- mandatory requirements **in bold**
- points can be binary (1) or fractioned e.g. (3/3):
    - 1/3 points = below average
    - 2/3 points = (slightly above) average
    - 1 point = exemplary

## Criteria to Review

- [ ] **project is licensed in compliance with either the [free software definition](https://www.gnu.org/philosophy/free-sw.html#fs-definition) or the [open source definition](https://opensource.org/osd)** (1)
    > see the [SPDX list](https://spdx.org/licenses/) for compliant licenses
- **README (or equivalent) is provided, covering at least a project description** (3/3)
- [ ] **project uses a versioning scheme** (e.g. git) (1)
- [ ] project is compliant with the [REUSE specification](https://reuse.software/) (1)
- [ ] all files are available in an editable format (2/2)
    - **source files** (1/2)
    - all files (except for exported binaries of course) (1)
- [ ] software relies on an open-source toolchain (2/2)
    - ehm, not (e.g. matlab script) (0)
    - for the core functionality (1/2)
    - alone (e.g. python instead of matlab) (1)
- [ ] code is documented (3/3)
- [ ] rationale is available, explaining design considerations (3/3)
- [ ] software environment is well defined (3/3)
- [ ] installation guide is available (2/2)
- [ ] user manual is available, also covering troubleshooting (3/3)
- [ ] testing routine/data is available to test whether the replication of the code was successful (3/3)
- [ ] contribution guide is provided (2/2)
- [ ] project has been independently replicated & deployed (2/2)
    - any sort of reference should be provided

