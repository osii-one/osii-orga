# <3D PRINTED PART(S)-TO-REVIEW>

The technical documentation shall contain sufficient information for the target group to replicate and modify self-designed parts.

OSI²'s Technology-specific Documentation Criteria (TsDC) for 3D printed parts are defined [here](https://gitlab.com/osii-one/osii-cab/-/blob/main/TsDC/3D-Prints.md).

> Note 1: All files must come in a file format that allows lossless editing. In the case of proprietary file formats, appropriate export formats shall be provided so that the information is accessible without needing to purchase for proprietary software licenses.

Compliance is assessed for the following parts:

## 3D-PRINTED-PART1

- [ ] approved by reviewer 1
- [ ] approved by reviewer 2

## 3D-PRINTED-PART2

- [ ] approved by reviewer 1
- [ ] approved by reviewer 2
