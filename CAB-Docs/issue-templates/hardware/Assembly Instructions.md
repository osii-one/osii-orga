# Assembly Instructions

The technical documentation shall include sufficient information
for the target group to assemble the design
and to make modifications to the assembly.

This includes:

- a complete bill of materials (BoM),
  - in which standard components (screws etc.) are unambiguously defined
    (e.g. `ISO 4032 M8, stainless steel` for a "standard" hexagonal stainless steel nut)
- dedicated documentation on how to assemble the design,
  e.g. given by an assembly drawing, assembly guide or videos

> Note 1:
> All files must come in a file format that allows lossless editing.
> In the case of proprietary file formats,
> appropriate export formats shall be provided
> so that the information is accessible
> without needing to purchase for proprietary software licenses.

> Note 2:
> Some designs may be trivial enough to come without dedicated assembly instructions,
> such as one-piece 3D-prints.
> Whether this is the case is subject to the decision of the reviewers.

- [ ] approved by reviewer 1
- [ ] approved by reviewer 2
