# <PART(S)-TO-REVIEW>

The technical documentation shall contain sufficient information
for the target group to replicate and modify self-designed parts.

> Note 1:
> All files must come in a file format that allows lossless editing.
> In the case of proprietary file formats,
> appropriate export formats shall be provided
> so that the information is accessible
> without needing to purchase for proprietary software licenses.

This is assessed for the following parts:

## PART1

- [ ] approved by reviewer 1
- [ ] approved by reviewer 2

## PART2

- [ ] approved by reviewer 1
- [ ] approved by reviewer 2
