# Licensing Terms

The project is licensed in compliance with the [OSHWA Definition](https://www.oshwa.org/definition/).

> find details on compliant licenses [here]()

- [ ] approved by reviewer 1
- [ ] approved by reviewer 2
