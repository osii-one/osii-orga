# Review of Hardware

## Note to Reviewers

- every criterion has to be checked by at least 2 reviewers; reviewers shall specify, which points they have checked
- mandatory requirements **in bold**
- reviewers may, in consesus, remove optional criteria if they're not applicable
- points can be binary (1) or fractioned e.g. (3/3):
    - 1/3 points = below average
    - 2/3 points = (slightly above) average
    - 1 point = exemplary

## Criteria to Review

- **project is licensed in compliance with the [OSHWA Definition](https://www.oshwa.org/definition/)** (1)
    > see [OSI² license reommendations](/TsDC/License-Recommendation.md)
    > see the [SPDX list](https://spdx.org/licenses/) for open-source licenses
- **README (or equivalent) is provided, covering at least a project description** (3/3)
- [ ] **project uses a versioning scheme** (e.g. git) (1)
- [ ] project is compliant with the [REUSE specification](https://reuse.software/) (1)
    > [OSH-tool](https://github.com/hoijui/osh-tool/) provides a helper tool
- [ ] **Bill of Materials is provided**, see [TsDC/BOM](/TsDC/BOM.md) for mandatory information and best practices (3/3)
- [ ] end-of-life components have been either fully avoided or highlighted + alternatives proposed (2/2)
- [ ] hardware relies on components that are either open-source, standardized or highly available (2/2)
    - ehm, not (assembly of mostly proprietary components) (0)
    - for the core functionality (1/2)
    - completely (1)
- [ ] manufacturing environment is well defined (3/3)
    > incl. manufacturing process, machine parameters etc.
    > see e.g. [TsDC/3D-Print](/TsDC/3D-Print.md#for-production)
- [ ] manufacturing guide is available (3/3)
- [ ] assembly guide is available (3/3)
    > this includes the entire setup of the project, e.g. calibration
- [ ] testing routine is available to confirm the expected performance (3/3)
- [ ] user manual is available (3/3)
- [ ] rationale is available, explaining design considerations (3/3)
- [ ] all files are available in an editable format (2/2)
    - **source files** (1/2)
    - all files (except for exported binaries of course) (1)
- [ ] all files can be losslessly edited with open-source software (2/2)
    - **source files** (1/2)
    - all files (except for exported binaries of course) (1)
- [ ] release bundle available (3/3)
    - **essential export/production files are provided for specific project version (e.g. STL, STP, GRB)** (not as a release bundle) (1/3)
        > best practice: avoid binaries in repos, use releases for export files (see [OSI² Contribution Guide](CONTRIBUTING.md#binaries) for details))
    - referanceable release bundle is available with essential export/production files, but is lacking important information for replication (2/3)
    - project can be completely replicated with the bundle alone (1)
- [ ] contribution guide is provided (2/2)
- [ ] project has been independently replicated & deployed (2/2)
    - any sort of reference should be provided

