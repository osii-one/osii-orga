---
title: Declaration of Freedom of Intellectual Property Restrictions
...

# Declaring Party

My details:

- Full Name:
- Project Name:
- URL to Open Source Project:

I hereby declare that I have the necessary representative competence
to make this declaration on behalf of the above stated open source project:\
I affirm that I am duly authorized and empowered to act as a representative of this project,
and I have the legal authority to provide the statements
and assurances contained within this declaration.

# Declaration of Freedom[^attribution]

[^attribution]: This text is based on mifactori's public promise ([CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/legalcode), <https://mifactori.de/design-rights/>).

In addition to the open source license for the copyright-based work of the project
(e.g. text, software code, images, videos)
I confirm that, to the best of my knowledge and information,
there are no registered patents or industrial design rights
or any other forms of intellectual property rights
(other than copyright) that would limit or impede the free use,
development, modification, distribution,
replication or commercialization of the said project.

I also declare to not make use of any of the unregistered design rights
that exist in relation of the project as of the date of signing –
neither to sue users nor to register design rights later on.

&nbsp;
&nbsp;
&nbsp;
&nbsp;
&nbsp;
&nbsp;
&nbsp;
&nbsp;
&nbsp;

Date:

Signature:

&nbsp;

\noindent\rule{4cm}{0.4pt}|
