# Open Source Imaging Initiative (OSI²) - Orga

[[_TOC_]]

## Overview

This is the organizational space for the Open Source Imaging Initiative
([www.opensourceimaging.org](https://www.opensourceimaging.org)).
If you want to contribute, have a look at the following information.

## Licensing

The content in this repository is licensed under [Creative Commons Attribution 4.0 International](LICENSE.md).\
If you share (parts of) this content (including in modified form), you must:

- give appropriate credit,
- provide a link to the license and
- indicate if changes were made.

Regarding credit, you may refer to the "Open Source Imaging Initiative"
(or short: "OSI²") as the sole representative –
instead of having to list all authors individually.

As a contributor to this project you become part of the OSI²
and thereby agree that your name may be omitted in the credits by the above stated form.

## Open-source Projects on Opensourceimaging.org

A list of current open-source projects can be found below. 

### Update project information

If you would like to update a project,
please create an issue with relevant information here:

### Add projects

If you would like to add your project to be highlighted on the website,
please use a template and add it to this folder here over a commit.

## List of open-source software

<!--- from https://docs.google.com/spreadsheets/d/1vor__7RM0dwkKThBPkF9By9ZQDBW4acuB4K5aU3etlY/edit?usp=sharing -->

| Project Name | Description | License | License Type | Untrapped | Platform | Reference |
|---|---|---|---|---|---|---|
| [3D Slicer](https://www.slicer.org/) | Image computing platform | 3D Slicer license | permissive | Yes | Github.com | [v5.4.0](https://github.com/Slicer/Slicer/tree/v5.4.0) |
| [AFNI](https://afni.nimh.nih.gov/) | Image postprocessing | GPL-2.0 | strong copyleft | Yes | Github.com | [AFNI_23.3.08](https://github.com/afni/afni/tree/AFNI_23.3.08) |
| [ANTs](https://github.com/ANTsX/ANTs) | Image postprocessing | Apache 2.0 | permissive | Yes | Github.com | [v2.5.0](https://github.com/ANTsX/ANTs/tree/v2.5.0) |
| [ANTsPy](https://github.com/ANTsX/ANTsPy) | Image postprocessing | Apache 2.0 | permissive | Yes | Github.com | [v0.4.2](https://github.com/ANTsX/ANTsPy/tree/v0.4.2) |
| [BART](https://mrirecon.github.io/bart/) | Image reconstruction | BSD-3-Clause | permissive | Yes | Github.com | [v0.8.00](https://github.com/mrirecon/bart/tree/v0.8.00) |
| [CoilGen](https://github.com/Philipp-MR/CoilGen) | Gradient coil design | GPL-3.0 | strong copyleft | No | Github.com | [commit 1732bd6](https://github.com/Philipp-MR/CoilGen/tree/1732bd6595267cae53c57317a87bee69189529ce) |
| [CoSimPy](https://github.com/umbertozanovello/CoSimPy) | Cricuit co-simulation | MIT | permissive | Yes | Github.com | [v1.4.1](https://github.com/umbertozanovello/CoSimPy/tree/v1.4.1) |
| [Dafne](https://www.dafne.network/) | Image segmentation | GPL-3.0 | strong copyleft | Yes | Github.com | [v1.7-alpha1](https://github.com/dafne-imaging/dafne/tree/1.7-alpha1) |
| [EPTlib](https://eptlib.github.io/) | Electric properties tomography | MIT | permissive | Yes | Github.com | [v0.3.3](https://github.com/EPTlib/eptlib/tree/v0.3.3) |
| [erwin](https://erwin.readthedocs.io/en/latest/) | Quantitative MRI | MIT | permissive | Yes | Github.com | [v1.2.0](https://github.com/lamyj/erwin/tree/v1.2.0) |
| [FreeSurfer](https://github.com/freesurfer/freesurfer) | Image postprocessing | FreeSurfer Software License Agreement | permissive | Yes | Github.com | [v7.4.1](https://github.com/freesurfer/freesurfer/tree/v7.4.1) |
| [Gadgetron](https://gadgetron.github.io/) | Image reconstruction | GADGETRON Software License (modified MIT license) | permissive | Yes | Github.com | [v4.1](https://github.com/gadgetron/gadgetron/tree/v4.1) |
| [GNURadio MRI](https://bitbucket.org/wgrissom/gnuradio-mri/src/master/) | Pulse sequences | GPL-3.0 | strong copyleft | Yes | bitbucket.org | [v3.7](https://bitbucket.org/wgrissom/gnuradio-mri/src/master/gr_3.7/gr-MRI/) |
| [HalbachOptimisation](https://github.com/LUMC-LowFieldMRI/HalbachOptimisation) | Magnet simulation | MIT | permissive | Yes | Github.com | [commit b5edf2e](https://github.com/LUMC-LowFieldMRI/HalbachOptimisation/tree/b5edf2e269dac6b10a03ce5f5b4dac391b5c44ba) |
| [hMRI-Toolbox](https://hmri-group.github.io/hMRI-toolbox/) | Quantitative MRI | GPL-2.0 | strong copyleft | No | Github.com | [v0.6.1](https://github.com/hMRI-group/hMRI-toolbox/tree/v0.6.1) |
| [ImageJ](https://imagej.net/software/imagej/) | Image postprocessing | Public Domain | permissive | Yes | Github.com | [v1.54f](https://github.com/imagej/ImageJ/tree/v1.54f) |
| [ISMRMRD](https://ismrmrd.readthedocs.io/en/latest/) | MR raw data format | ISMRMRD Software License (modified MIT license) | permissive | Yes | Github.com | [v1.13.7](https://github.com/ismrmrd/ismrmrd/tree/v1.13.7) |
| [ITK](https://itk.org/) | Image postprocessing | Apache 2.0 | permissive | Yes | Github.com | [v5.3.0](https://github.com/InsightSoftwareConsortium/ITK/tree/v5.3.0) |
| [JEMRIS](https://www.jemris.org/) | MRI simulations | GPL-2.0 | strong copyleft | No | Github.com | [v2.9.1](https://github.com/JEMRIS/jemris/tree/v2.9.1) |
| [Keras](https://keras.io/) | Deep learning | Apache-2.0        | permissive | Yes | Github.com | [v3.0.0](https://github.com/keras-team/keras/tree/v3.0.0) |
| [KomaMRI.jl](https://cncastillo.github.io/KomaMRI.jl/stable/) | MRI simulations | MIT | permissive | Yes | Github.com | [v0.7.5](https://github.com/cncastillo/KomaMRI.jl/tree/v0.7.5) |
| [LUMC Gradient Design Tool](https://github.com/LUMC-LowFieldMRI/GradientDesignTool) | Gradient coil simulation | MIT | permissive | Yes | Github.com | [1a2dd56](https://github.com/LUMC-LowFieldMRI/GradientDesignTool/tree/1a2dd561ff4573fdb681462a5bb0832e11614b8d) |
| [Madym](https://gitlab.com/manchester_qbi/manchester_qbi_public/madym_cxx) | Quantitative MRI | Apache-2.0        | permissive | Yes | Gitlab.com | [v4.23.0](https://gitlab.com/manchester_qbi/manchester_qbi_public/madym_cxx/-/tree/v4.23.0) |
| [MaRGA](https://github.com/vnegnev/marga) | Console software | GPL-3.0 | strong copyleft | Yes | Github.com | [commit 35c599e](https://github.com/vnegnev/marga/tree/35c599e66751e2dcbc4f7e0b7fd4a18e47114385) |
| [MaRCoS_extras](https://github.com/vnegnev/marcos_extras) | Console software | GPL-3.0 | strong copyleft | Yes | Github.com | [commit f5a9761](https://github.com/vnegnev/marcos_extras/tree/f5a97615b047b2c114fc11821e9b6d8f948eb4f8) |
| [MARIE](https://github.com/thanospol/MARIE) | EM field simulations | GPL-2.0 | strong copyleft | No | Github.com | [commit 3254454](https://github.com/thanospol/MARIE/tree/3254454a95c7ab5f2dbb9da3033731bbf0c5c79c) |
| [MIRTK](https://mirtk.github.io/) | Image postprocessing | Apache 2.0 | permissive | Yes | Github.com | [v2.0](https://github.com/BioMedIA/MIRTK/tree/v2.0.0) |
| [MONAI](https://monai.io/) | Deep learning | Apache-2.0        | permissive | Yes | Github.com | [v1.3.0](https://github.com/Project-MONAI/MONAI/tree/1.3.0) |
| [MRILab](https://mrilab.sourceforge.net/) | MRI simulations | BSD-2-Clause        | permissive | No | Github.com | [v1.3](https://github.com/leoliuf/MRiLab/tree/v1.3) |
| [MRIReco.jl](https://magneticresonanceimaging.github.io/MRIReco.jl/latest/) | Image reconstruction | MIT | permissive | Yes | Github.com | [v0.7.1](https://github.com/MagneticResonanceImaging/MRIReco.jl/tree/v0.8.0) |
| [NiftyReg](https://github.com/KCL-BMEIS/niftyreg) | Image postprocessing | BSD-3-Clause | permissive | Yes | Github.com | [commit 6db8b16](https://github.com/KCL-BMEIS/niftyreg/tree/6db8b16c17884a9b8859c980c4f1c408f62bd9ca) |
| [OCRA](https://openmri.github.io/ocra/) | Console software | GPL-3.0 | strong copyleft | Yes | Github.com | [commit c5ce93c](https://github.com/OpenMRI/ocra/tree/c5ce93c2846c260f658dafe74e773212e2b979df) |
| [ODIN](https://od1n.sourceforge.net/) | Development interface | GPL-2.0 | strong copyleft | Yes | sourceforge.net | [v2.0.5](https://sourceforge.net/p/od1n/code/9735/tree/) |
| [Pulseq](https://pulseq.github.io/) | Pulse sequences | MIT | permissive | No | Github.com | [v1.4.0](https://github.com/pulseq/pulseq/tree/v1.4.1) |
| [PyCoilGen](https://github.com/kev-m/pyCoilGen) | Gradient coil design | GPL-3.0 | strong copyleft | Yes | Github.com | [v0.2.0](https://github.com/kev-m/pyCoilGen/tree/0.2.0) |
| [PyPulseq](https://pypulseq.readthedocs.io/en/master/) | Pulse sequences | AGPL-3.0 | strong copyleft | Yes | Github.com | [v1.4.0](https://github.com/imr-framework/pypulseq/tree/1.4.0) |
| [PyQMRI](https://github.com/IMTtugraz/PyQMRI) | Quantitative MRI | Apache-2.0        | permissive | Yes | Github.com | [v1.2.2](https://github.com/IMTtugraz/PyQMRI/tree/v1.2.2) |
| [PyTorch](https://pytorch.org/) | Deep learning | own license | permissive | Yes | Github.com | [v2.1.2](https://github.com/pytorch/pytorch/tree/v2.1.2) |
| [QMRITools](https://www.qmritools.com/) | Quantitative MRI | BSD-3-Clause | permissive | No | Github.com | [v3.10.0](https://github.com/mfroeling/QMRITools/tree/3.10.0) |
| [qMRLab](https://qmrlab.org/) | Quantitative MRI | MIT | permissive | Yes | Github.com | [v2.5.0b](https://github.com/qMRLab/qMRLab/tree/v2.5.0b) |
| [QUIT](https://github.com/spinicist/QUIT) | Quantitative MRI | MPL-2.0 | weak copyleft | Yes | Github.com | [v3.3](https://github.com/spinicist/QUIT/tree/v3.4) |
| [Scanhub](https://brain-link.github.io/scanhub/) | Console software | GPL-3.0 | strong copyleft | Yes | Github.com | [commit 7a04694](https://github.com/brain-link/scanhub/tree/7a04694b33b14d0e2ddbe45556d33863d857fda0) |
| [SEPIA](https://sepia-documentation.readthedocs.io/en/latest/) | Susceptibility mapping | MIT | permissive | No | Github.com | [v1.2.2.4](https://github.com/kschan0214/sepia/tree/v1.2.2.6) |
| [Shimming Toolbox](https://shimming-toolbox.org/) | B1/B0 shimming | GPL-3.0 | strong copyleft | Yes | Github.com | [v0.1](https://github.com/shimming-toolbox/shimming-toolbox/tree/v0.1) |
| [SigPy](https://github.com/mikgroup/sigpy) | MRI simulations | BSD-3-Clause | permissive | Yes | Github.com | [v0.1.25](https://github.com/mikgroup/sigpy/tree/v0.1.25) |
| [SimpleElastix](https://simpleelastix.github.io/) | Image postprocessing | Apache 2.0 | permissive | Yes | Github.com | [v1.1.0](https://github.com/SuperElastix/SimpleElastix/tree/v1.1.0) |
| [SIRF](https://www.ccpsynerbi.ac.uk/) | Image reconstruction | Apache-2.0        | permissive | Yes | Github.com | [v3.4.0](https://github.com/SyneRBI/SIRF/tree/v3.5.0) |
| [SlicerElastix](https://github.com/lassoan/SlicerElastix) | Extension for 3D Slicer | MIT | Permissive | Yes | Github.com | [commit ed32bee](https://github.com/lassoan/SlicerElastix/tree/ed32bee7654440e924f586fae00580de517c9244) |
| [Spinach](http://spindynamics.org/Spinach.php/) | MRI simulations | MIT | permissive | No | own website | [v2.8.6280](https://www.dropbox.com/scl/fi/nfejfrru32o9neakpa3hg/spinach_2_8_6280.zip) |
| [SPM](https://www.fil.ion.ucl.ac.uk/spm/) | Quantitative MRI | GPL-2.0 | strong copyleft | No | Github.com | [commit 91af6ea](https://github.com/spm/spm/tree/91af6ea203df3ef614882a947041915c790e2ad7) |
| [ThinWire – MRI Gradient Coil Design](https://github.com/Sebastian-MR/ThinWire_MRIGradientCoilDesign) | Gradient coil simulation | MIT | permissive | No | Github.com | [commit 9f020c9](https://github.com/Sebastian-MR/ThinWire_MRIGradientCoilDesign/tree/9f020c9cf32e9bca7d307cae287fb567c3bd8c6b) |
| [TOPPE](https://github.com/toppeMRI/toppe) | Pulse sequences | MIT | permissive | No | Github.com | [v3.0](https://github.com/toppeMRI/toppe/tree/v3.0) |
| [Vespa MRS](https://vespa-mrs.github.io/vespa.io/) | MRI simulations | BSD-3-Clause | permissive | Yes | Github.com | [commit b5e6946](https://github.com/vespa-mrs/vespa/tree/b5e6946ed535dc3d763e178e7de26cd60dff9352) |
| [Virtual MRI scanner](https://github.com/imr-framework/virtual-scanner) | MRI simulations | AGPL-3.0 | strong copyleft | Yes | Github.com | [v2.0.0](https://github.com/imr-framework/virtual-scanner/tree/2.0.0) |
| [Yarra Framework](https://yarra-framework.org/) | Image reconstruction | GPL-3.0 | strong copyleft | Yes | bitbucket.org | [client v0.62, server v0.97](https://bitbucket.org/yarra-dev/yarraclient/commits/tag/v0.62) |

<!--- use https://tableconvert.com/ to generate the JSON data from e.g. CSV or clipboard (JSON Format: Array of Object) and then just replace everything _within_ `"items" :`

```json:table

{
    "fields" : [
        {"key": "Project Name", "sortable": true},
        {"key": "Description"},
        {"key": "License", "sortable": true},
        {"key": "License Type", "sortable": true},
        {"key": "Untrapped", "sortable": true},
        {"key": "Platform", "sortable": true},
        {"key": "Reference"}
	],
    "items" : [
        {
        "Project Name": "3D Slicer",
        "Description": "Image computing platform",
        "License": "3D Slicer license",
        "License Type": "permissive",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "https://github.com/Slicer/Slicer/tree/v5.4.0"
    },
    {
        "Project Name": "AFNI",
        "Description": "Image postprocessing",
        "License": "GPL-2.0",
        "License Type": "strong copyleft",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[AFNI_23.3.08](https://github.com/afni/afni/tree/AFNI_23.3.08)"
    },
    {
        "Project Name": "ANTs",
        "Description": "Image postprocessing",
        "License": "Apache 2.0",
        "License Type": "permissive",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[v2.5.0](https://github.com/ANTsX/ANTs/tree/v2.5.0)"
    },
    {
        "Project Name": "ANTsPy",
        "Description": "Image postprocessing",
        "License": "Apache 2.0",
        "License Type": "permissive",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[v0.4.2](https://github.com/ANTsX/ANTsPy/tree/v0.4.2)"
    },
    {
        "Project Name": "BART",
        "Description": "Image reconstruction",
        "License": "BSD-3-Clause",
        "License Type": "permissive",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[v0.8.00](https://github.com/mrirecon/bart/tree/v0.8.00)"
    },
    {
        "Project Name": "CoilGen",
        "Description": "Gradient coil design",
        "License": "GPL-3.0",
        "License Type": "strong copyleft",
        "Untrapped": "No",
        "Platform": "Github.com",
        "Reference": "[commit 1732bd6](https://github.com/Philipp-MR/CoilGen/tree/1732bd6595267cae53c57317a87bee69189529ce)"
    },
    {
        "Project Name": "CoSimPy",
        "Description": "Cricuit co-simulation",
        "License": "MIT",
        "License Type": "permissive",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[v1.4.1](https://github.com/umbertozanovello/CoSimPy/tree/v1.4.1)"
    },
    {
        "Project Name": "Dafne",
        "Description": "Image segmentation",
        "License": "GPL-3.0",
        "License Type": "strong copyleft",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[v1.7-alpha1](https://github.com/dafne-imaging/dafne/tree/1.7-alpha1)"
    },
    {
        "Project Name": "EPTlib",
        "Description": "Electric properties tomography",
        "License": "MIT",
        "License Type": "permissive",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[v0.3.3](https://github.com/EPTlib/eptlib/tree/v0.3.3)"
    },
    {
        "Project Name": "erwin",
        "Description": "Quantitative MRI",
        "License": "MIT",
        "License Type": "permissive",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[v1.2.0](https://github.com/lamyj/erwin/tree/v1.2.0)"
    },
    {
        "Project Name": "FreeSurfer",
        "Description": "Image postprocessing",
        "License": "FreeSurfer Software License Agreement",
        "License Type": "permissive",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[v7.4.1](https://github.com/freesurfer/freesurfer/tree/v7.4.1)"
    },
    {
        "Project Name": "Gadgetron",
        "Description": "Image reconstruction",
        "License": "GADGETRON Software License (modified MIT license)",
        "License Type": "permissive",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[v4.1](https://github.com/gadgetron/gadgetron/tree/v4.1)"
    },
    {
        "Project Name": "GNURadio MRI",
        "Description": "Pulse sequences",
        "License": "GPL-3.0",
        "License Type": "strong copyleft",
        "Untrapped": "Yes",
        "Platform": "bitbucket.org",
        "Reference": "[v3.7](https://bitbucket.org/wgrissom/gnuradio-mri/src/master/gr_3.7/gr-MRI/)"
    },
    {
        "Project Name": "HalbachOptimisation",
        "Description": "Magnet simulation",
        "License": "MIT",
        "License Type": "permissive",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[commit b5edf2e](https://github.com/LUMC-LowFieldMRI/HalbachOptimisation/tree/b5edf2e269dac6b10a03ce5f5b4dac391b5c44ba)"
    },
    {
        "Project Name": "hMRI-Toolbox",
        "Description": "Quantitative MRI",
        "License": "GPL-2.0",
        "License Type": "strong copyleft",
        "Untrapped": "No",
        "Platform": "Github.com",
        "Reference": "[v0.6.1](https://github.com/hMRI-group/hMRI-toolbox/tree/v0.6.1)"
    },
    {
        "Project Name": "ImageJ",
        "Description": "Image postprocessing",
        "License": "Public Domain",
        "License Type": "permissive",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[v1.54f](https://github.com/imagej/ImageJ/tree/v1.54f)"
    },
    {
        "Project Name": "ISMRMRD",
        "Description": "MR raw data format",
        "License": "ISMRMRD Software License (modified MIT license)",
        "License Type": "permissive",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[v1.13.7](https://github.com/ismrmrd/ismrmrd/tree/v1.13.7)"
    },
    {
        "Project Name": "ITK",
        "Description": "Image postprocessing",
        "License": "Apache 2.0",
        "License Type": "permissive",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[v5.3.0](https://github.com/InsightSoftwareConsortium/ITK/tree/v5.3.0)"
    },
    {
        "Project Name": "JEMRIS",
        "Description": "MRI simulations",
        "License": "GPL-2.0",
        "License Type": "strong copyleft",
        "Untrapped": "No",
        "Platform": "Github.com",
        "Reference": "[v2.9.1](https://github.com/JEMRIS/jemris/tree/v2.9.1)"
    },
    {
        "Project Name": "Keras",
        "Description": "Deep learning",
        "License": "Apache-2.0       ",
        "License Type": "permissive",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[v3.0.0](https://github.com/keras-team/keras/tree/v3.0.0)"
    },
    {
        "Project Name": "KomaMRI.jl",
        "Description": "MRI simulations",
        "License": "MIT",
        "License Type": "permissive",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[v0.7.5](https://github.com/cncastillo/KomaMRI.jl/tree/v0.7.5)"
    },
    {
        "Project Name": "LUMC Gradient Design Tool",
        "Description": "Gradient coil simulation",
        "License": "MIT",
        "License Type": "permissive",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[1a2dd56](https://github.com/LUMC-LowFieldMRI/GradientDesignTool/tree/1a2dd561ff4573fdb681462a5bb0832e11614b8d)"
    },
    {
        "Project Name": "Madym",
        "Description": "Quantitative MRI",
        "License": "Apache-2.0       ",
        "License Type": "permissive",
        "Untrapped": "Yes",
        "Platform": "Gitlab.com",
        "Reference": "[v4.23.0](https://gitlab.com/manchester_qbi/manchester_qbi_public/madym_cxx/-/tree/v4.23.0)"
    },
    {
        "Project Name": "MaRGA",
        "Description": "Console software",
        "License": "GPL-3.0",
        "License Type": "strong copyleft",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[commit 35c599e](https://github.com/vnegnev/marga/tree/35c599e66751e2dcbc4f7e0b7fd4a18e47114385)"
    },
    {
        "Project Name": "MaRCoS_extras",
        "Description": "Console software",
        "License": "GPL-3.0",
        "License Type": "strong copyleft",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[commit f5a9761](https://github.com/vnegnev/marcos_extras/tree/f5a97615b047b2c114fc11821e9b6d8f948eb4f8)"
    },
    {
        "Project Name": "MARIE",
        "Description": "EM field simulations",
        "License": "GPL-2.0",
        "License Type": "strong copyleft",
        "Untrapped": "No",
        "Platform": "Github.com",
        "Reference": "[commit 3254454](https://github.com/thanospol/MARIE/tree/3254454a95c7ab5f2dbb9da3033731bbf0c5c79c)"
    },
    {
        "Project Name": "MIRTK",
        "Description": "Image postprocessing",
        "License": "Apache 2.0",
        "License Type": "permissive",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[v2.0](https://github.com/BioMedIA/MIRTK/tree/v2.0.0)"
    },
    {
        "Project Name": "MONAI",
        "Description": "Deep learning",
        "License": "Apache-2.0       ",
        "License Type": "permissive",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[v1.3.0](https://github.com/Project-MONAI/MONAI/tree/1.3.0)"
    },
    {
        "Project Name": "MRILab",
        "Description": "MRI simulations",
        "License": "BSD-2-Clause       ",
        "License Type": "permissive",
        "Untrapped": "No",
        "Platform": "Github.com",
        "Reference": "[v1.3](https://github.com/leoliuf/MRiLab/tree/v1.3)"
    },
    {
        "Project Name": "MRIReco.jl",
        "Description": "Image reconstruction",
        "License": "MIT",
        "License Type": "permissive",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[v0.7.1](https://github.com/MagneticResonanceImaging/MRIReco.jl/tree/v0.8.0)"
    },
    {
        "Project Name": "NiftyReg",
        "Description": "Image postprocessing",
        "License": "BSD-3-Clause",
        "License Type": "permissive",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[commit 6db8b16](https://github.com/KCL-BMEIS/niftyreg/tree/6db8b16c17884a9b8859c980c4f1c408f62bd9ca)"
    },
    {
        "Project Name": "OCRA",
        "Description": "Console software",
        "License": "GPL-3.0",
        "License Type": "strong copyleft",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[commit c5ce93c](https://github.com/OpenMRI/ocra/tree/c5ce93c2846c260f658dafe74e773212e2b979df)"
    },
    {
        "Project Name": "ODIN",
        "Description": "Development interface",
        "License": "GPL-2.0",
        "License Type": "strong copyleft",
        "Untrapped": "Yes",
        "Platform": "sourceforge.net",
        "Reference": "[v2.0.5](https://sourceforge.net/p/od1n/code/9735/tree/)"
    },
    {
        "Project Name": "Pulseq",
        "Description": "Pulse sequences",
        "License": "MIT",
        "License Type": "permissive",
        "Untrapped": "No",
        "Platform": "Github.com",
        "Reference": "[v1.4.0](https://github.com/pulseq/pulseq/tree/v1.4.1)"
    },
    {
        "Project Name": "PyCoilGen",
        "Description": "Gradient coil design",
        "License": "GPL-3.0",
        "License Type": "strong copyleft",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[v0.2.0](https://github.com/kev-m/pyCoilGen/tree/0.2.0)"
    },
    {
        "Project Name": "PyPulseq",
        "Description": "Pulse sequences",
        "License": "AGPL-3.0",
        "License Type": "strong copyleft",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[v1.4.0](https://github.com/imr-framework/pypulseq/tree/1.4.0)"
    },
    {
        "Project Name": "PyQMRI",
        "Description": "Quantitative MRI",
        "License": "Apache-2.0       ",
        "License Type": "permissive",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[v1.2.2](https://github.com/IMTtugraz/PyQMRI/tree/v1.2.2)"
    },
    {
        "Project Name": "PyTorch",
        "Description": "Deep learning",
        "License": "own license",
        "License Type": "permissive",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[v2.1.2](https://github.com/pytorch/pytorch/tree/v2.1.2)"
    },
    {
        "Project Name": "QMRITools",
        "Description": "Quantitative MRI",
        "License": "BSD-3-Clause",
        "License Type": "permissive",
        "Untrapped": "No",
        "Platform": "Github.com",
        "Reference": "[v3.10.0](https://github.com/mfroeling/QMRITools/tree/3.10.0)"
    },
    {
        "Project Name": "qMRLab",
        "Description": "Quantitative MRI",
        "License": "MIT",
        "License Type": "permissive",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[v2.5.0b](https://github.com/qMRLab/qMRLab/tree/v2.5.0b)"
    },
    {
        "Project Name": "QUIT",
        "Description": "Quantitative MRI",
        "License": "MPL-2.0",
        "License Type": "weak copyleft",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[v3.3](https://github.com/spinicist/QUIT/tree/v3.4)"
    },
    {
        "Project Name": "Scanhub",
        "Description": "Console software",
        "License": "GPL-3.0",
        "License Type": "strong copyleft",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[commit 7a04694](https://github.com/brain-link/scanhub/tree/7a04694b33b14d0e2ddbe45556d33863d857fda0)"
    },
    {
        "Project Name": "SEPIA",
        "Description": "Susceptibility mapping",
        "License": "MIT",
        "License Type": "permissive",
        "Untrapped": "No",
        "Platform": "Github.com",
        "Reference": "[v1.2.2.4](https://github.com/kschan0214/sepia/tree/v1.2.2.6)"
    },
    {
        "Project Name": "Shimming Toolbox",
        "Description": "B1/B0 shimming",
        "License": "GPL-3.0",
        "License Type": "strong copyleft",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[v0.1](https://github.com/shimming-toolbox/shimming-toolbox/tree/v0.1)"
    },
    {
        "Project Name": "SigPy",
        "Description": "MRI simulations",
        "License": "BSD-3-Clause",
        "License Type": "permissive",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[v0.1.25](https://github.com/mikgroup/sigpy/tree/v0.1.25)"
    },
    {
        "Project Name": "SimpleElastix",
        "Description": "Image postprocessing",
        "License": "Apache 2.0",
        "License Type": "permissive",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[v1.1.0](https://github.com/SuperElastix/SimpleElastix/tree/v1.1.0)"
    },
    {
        "Project Name": "SIRF",
        "Description": "Image reconstruction",
        "License": "Apache-2.0       ",
        "License Type": "permissive",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[v3.4.0](https://github.com/SyneRBI/SIRF/tree/v3.5.0)"
    },
    {
        "Project Name": "SlicerElastix",
        "Description": "Extension for 3D Slicer",
        "License": "MIT",
        "License Type": "Permissive",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[commit ed32bee](https://github.com/lassoan/SlicerElastix/tree/ed32bee7654440e924f586fae00580de517c9244)"
    },
    {
        "Project Name": "Spinach",
        "Description": "MRI simulations",
        "License": "MIT",
        "License Type": "permissive",
        "Untrapped": "No",
        "Platform": "own website",
        "Reference": "[v2.8.6280](https://www.dropbox.com/scl/fi/nfejfrru32o9neakpa3hg/spinach_2_8_6280.zip)"
    },
    {
        "Project Name": "SPM",
        "Description": "Quantitative MRI",
        "License": "GPL-2.0",
        "License Type": "strong copyleft",
        "Untrapped": "No",
        "Platform": "Github.com",
        "Reference": "[commit 91af6ea](https://github.com/spm/spm/tree/91af6ea203df3ef614882a947041915c790e2ad7)"
    },
    {
        "Project Name": "ThinWire – MRI Gradient Coil Design",
        "Description": "Gradient coil simulation",
        "License": "MIT",
        "License Type": "permissive",
        "Untrapped": "No",
        "Platform": "Github.com",
        "Reference": "[commit 9f020c9](https://github.com/Sebastian-MR/ThinWire_MRIGradientCoilDesign/tree/9f020c9cf32e9bca7d307cae287fb567c3bd8c6b)"
    },
    {
        "Project Name": "TOPPE",
        "Description": "Pulse sequences",
        "License": "MIT",
        "License Type": "permissive",
        "Untrapped": "No",
        "Platform": "Github.com",
        "Reference": "[v3.0](https://github.com/toppeMRI/toppe/tree/v3.0)"
    },
    {
        "Project Name": "Vespa MRS",
        "Description": "MRI simulations",
        "License": "BSD-3-Clause",
        "License Type": "permissive",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[commit b5e6946](https://github.com/vespa-mrs/vespa/tree/b5e6946ed535dc3d763e178e7de26cd60dff9352)"
    },
    {
        "Project Name": "Virtual MRI scanner",
        "Description": "MRI simulations",
        "License": "AGPL-3.0",
        "License Type": "strong copyleft",
        "Untrapped": "Yes",
        "Platform": "Github.com",
        "Reference": "[v2.0.0](https://github.com/imr-framework/virtual-scanner/tree/2.0.0)"
    },
    {
        "Project Name": "Yarra Framework",
        "Description": "Image reconstruction",
        "License": "GPL-3.0",
        "License Type": "strong copyleft",
        "Untrapped": "Yes",
        "Platform": "bitbucket.org",
        "Reference": "[client v0.62, server v0.97](https://bitbucket.org/yarra-dev/yarraclient/commits/tag/v0.62)"
    }
	],
	"filter" : true,
	"caption" : "open-source software for MRI"
}
```
-->

### Pending

- [MaRCoS Server](https://github.com/vnegnev/marcos_server)
  - no license as by commit `133aa76`
- [MaRCoS Client](https://github.com/vnegnev/marcos_client)
  - no license as by commit `d9c80bd`

## List of open-source hardware

<!--- from https://docs.google.com/spreadsheets/d/1vor__7RM0dwkKThBPkF9By9ZQDBW4acuB4K5aU3etlY/edit?usp=sharing -->

| Project Name | Description | License | License Type | Native Files | Export Files | Bill of Materials | Instructions | Platform | Reference |
|---|---|---|---|---|---|---|---|---|---|
| [32-channel RF receive chain amplifiers](https://github.com/dezanche/32-channel_RF_system) | Modular 32-channel RF signal amplifier and attenuator | CERN-OHL-1.2 | strong copyleft | Yes | Yes | Yes | Yes | Github.com | [commit 2cae1f8](https://github.com/dezanche/32-channel_RF_system/tree/2cae1f8a17e2e8af4369a9399911cc1167c01fe9) |
| [8ch TX/RX RF coil 3T](https://gitlab1.ptb.de/mri-lab/8ch-ptx-headcoil-3t) | 8-channel 3T pTx head coil | CERN-OHL-W-2.0 | weak copyleft | Yes | Yes | Yes | Yes | PTB-hosted Gitlab | [commit d02caee3](https://gitlab1.ptb.de/mri-lab/8ch-ptx-headcoil-3t/-/tree/d02caee3eeff4a6d223c74e860a10a1aa6dbb453) |
| [8ch TX/RX RF coil 7T](https://gitlab1.ptb.de/mri-lab/8ch-ptx-headcoil-7t) | 8-channel 7T pTx head coil | CERN-OHL-W-2.0 | weak copyleft | Yes | Yes | No | Yes | PTB-hosted Gitlab | [commit e6033e92](https://gitlab1.ptb.de/mri-lab/8ch-ptx-headcoil-7t/-/tree/e6033e92ce2374ee67c40bbe584813e104f6edaa) |
| [COSI Measure](https://github.com/opensourceimaging/cosi-measure) | 3-axis measurement system | CERN-OHL-1.2 | strong copyleft | Yes | Yes | Yes | Yes | Github.com | [v1.0](https://github.com/opensourceimaging/cosi-measure/tree/223aed325aeb5dffe3ebcb21c4bbc3dcf3c00471) |
| [COSI transmit](https://github.com/opensourceimaging/cosi-transmit) | RF power amplifier 1kW | CERN-OHL-1.2 | strong copyleft | Yes | Yes | Yes | Yes | Github.com | [commit fee73e7](https://github.com/opensourceimaging/cosi-transmit/tree/fee73e7418bdffd0338575a901670c203c071c5b) |
| [GPA-FHDO](https://github.com/menkueclab/GPA-FHDO) | B0 shim or gradient amplifier | CERN-OHL-1.2 | strong copyleft | Yes | Yes | Yes | Yes | Github.com | [v1.2](https://github.com/menkueclab/GPA-FHDO/tree/v1.2) |
| [GPA-RP-Adapter](https://github.com/menkueclab/GPA-RP-Adapter) | Red pitaya shield to connect GPA-FHDO | CERN-OHL-W-2.0 | weak copyleft | Yes | No | Yes | No | Github.com | [commit a19ebca](https://github.com/menkueclab/GPA-RP-Adapter/tree/a19ebcac98d226faf494c5f480bc912eae1e7aad) |
| [H-field RF Probe](https://github.com/dezanche/H-field_RF_probe) | Magnetic field probe | CERN-OHL-1.2 | strong copyleft | Yes | Yes | Yes | - | Github.com | [v2.0](https://github.com/dezanche/H-field_RF_probe/tree/v2.0) |
| [High peak/average power TR-switch for thermal MR](https://github.com/opensourceimaging/tr-switch) | Transmit/receive switch | CERN-OHL-1.2 | strong copyleft | Yes | Yes | Yes | Yes | Github.com | [v1.0](https://github.com/opensourceimaging/tr-switch/tree/5479274b12ffe68bc166b205807871cc23afeb75) |
| [LimeSDR](https://limemicro.com/products/boards/limesdr/) | NMR console hardware | CC-BY-3.0 | permissive | Yes | Yes | Yes | Yes | Github.com | [1v4](https://github.com/myriadrf/LimeSDR-USB/tree/c6ea7cea27b90e94625b8e49b71ee2a10a5345bb) |
| [Maker-Gradient-Amplifier](https://github.com/mtwieg/Maker-Gradient-Amplifier) | Gradient amplifier | MIT | permissive | No | Yes | Yes | No | Github.com | [commit 0f4e5e3](https://github.com/mtwieg/Maker-Gradient-Amplifier/tree/0f4e5e39715806dcded848ff9c759b22680870a3) |
| [MRI cage test set](https://github.com/Astrei/MRI_Cage_test_set) | Antenna set to measure attenuation of Faraday RF cages | CERN-OHL-W-2.0 | weak copyleft | No | Yes | No | Yes | Github.com | [commit 3452e02](https://github.com/Astrei/MRI_Cage_test_set/tree/3452e02e82967d754d4f017bf1e0f47c9f0d53c7) |
| [MRI compatible wireless sensors for motion tracking](https://github.com/nyu-cbi-sensors/MRI-compatible-sensors) | MRI compatible wireless sensors for motion tracking | MIT | permissive | Yes | - | Yes | Yes | Github.com | [commit 1dd0414](https://github.com/nyu-cbi-sensors/MRI-compatible-sensors/tree/1dd0414ad9d5b4b014b462888586299bf88581e1) |
| [MRI distortion phantom](https://github.com/dezanche/MRI_distortion_phantom) | Modular phantom to measure distortion | CERN-OHL-W-2.0 | weak copyleft | Yes | No | Yes | Yes | Github.com | [commit 4896369](https://github.com/dezanche/MRI_distortion_phantom/tree/48963694eeb07e0483b7e756ffd82955f7d37be2) |
| [OCRA concole power supply](https://zeugmatographix.org/ocra/2023/11/07/power-supply-of-the-ocra-console/) | power supply for the OCRA concsole | CERN-OHL-W-2.0 | weak copyleft | No | Yes | Yes | Yes | self-hosted download link | [Rev001](https://data.stimulate.ovgu.de/f/e6e8eecdd4bd4430b90a/) |
| [OCRA RF rack](https://zeugmatographix.org/ocra/2022/04/18/rf-coil-for-the-ocra-tabletop-mri-system/) | RF coil for the OCRA tabletop system | CERN-OHL-W-2.0 | weak copyleft | No | Yes | Yes | Yes | self-hosted download link | [Rev002](https://data.stimulate.ovgu.de/f/89d4f1929fdd40c5be6d/) |
| [OCRA TR-Switch](https://zeugmatographix.org/ocra/2021/09/30/transmit-receive-switch-for-the-ocra-tabletop-mri-system/) | Transmit/receive switch | CERN-OHL-W-2.0 | weak copyleft | No | Yes | Yes | Yes | self-hosted download link | [Rev005](https://data.stimulate.ovgu.de/f/b4312a52c4fc445fb9f0/) |
| [OCRA1](https://zeugmatographix.org/ocra/2020/11/27/ocra1-spi-controlled-4-channel-18bit-dac-and-rf-attenutator/) | 4-channel digital-to-analog converter to play out gradient waveforms | CERN-OHL-W-2.0 | weak copyleft | No | Yes | Yes | Yes | self-hosted download link | [Rev003](https://data.stimulate.ovgu.de/f/76608ccbde02477abdc8/) |
| [Open Source NIST Phantom](https://github.com/kalinared/open-source-nist-phantom) | MR phantom | GPL-3.0 | strong copyleft | No | Yes | Yes | Yes | Github.com | [commit 251e5bd](https://github.com/kalinared/open-source-nist-phantom/tree/48acd253ed05e8f826c2958c804b867426e60625) |
| [OpenForce MR](https://github.com/fsantini/OpenForceMR) | MR compatible force sensor | GPL-3.0 | strong copyleft | Yes | Yes | Yes | Yes | Github.com | [commit d0c5d28](https://github.com/fsantini/OpenForceMR/tree/d0c5d2818555c91b1d8da37a5c4f5d62220b0a0d) |
| [OSI² ONE Magnet](https://gitlab.com/osii/magnet/30cm-halbach-magnet) | MR magnet, 48mT, Ø30cm bore | CERN-OHL-W-2.0 | weak copyleft | Yes | Yes | No | No | Gitlab.com | [v1.0.0](https://gitlab.com/osii/magnet/30cm-halbach-magnet/-/tree/v1.0.0) |
| [OSI² ONE RFPA](https://gitlab.com/osii/rf-system/rf-power-amplifier/1kw-peak-rfpa) | RF power amplifier 1kW | CERN-OHL-W-2.0 | weak copyleft | Yes | Yes | Yes | Yes | Gitlab.com | [commit 1b60ec3c](https://gitlab.com/osii/rf-system/rf-power-amplifier/1kw-peak-rfpa/-/tree/1b60ec3c0556c091fec209164f96deeb6918b7e1) |
| [Pneumatic phantom](https://osf.io/acgjk/) | inflation system to mimic respiration-induced MRI artefacts | MIT | permissive | Partially | Partially | No | No | OSF.io | [Rev 30.03.2022 18:07 UTC]() |
| [pTx implant safety testbed](https://gitlab1.ptb.de/mri-lab/ptx-implant-safety-testbed) | Parallel transmission hardware | CERN-OHL-W-2.0 | weak copyleft | Yes | - | Yes | Yes | PTB-hosted Gitlab | [commit 2e66f5db](https://gitlab1.ptb.de/mri-lab/ptx-implant-safety-testbed/-/tree/2e66f5db2797ddb85e13a6f27e21036700159e02) |
| [RedPitConsole](https://gitlab.com/osii/console/redpitconsole) | MRI console hardware | CERN-OHL-W-2.0 | weak copyleft | Yes | Yes | Yes | Yes | Gitlab.com | [v1.0.0](https://gitlab.com/osii/console/redpitconsole/-/tree/v1.0.0) |
| [Solenoid RF coil](https://gitlab.com/osii/rf-system/rf-coils/solenoid-d205mm-n20-3seg) | 20.5cm diameter RF coil | CERN-OHL-W-2.0 | weak copyleft | Yes | Yes | No | No | Gitlab.com | [commit da7e5dd4](https://gitlab.com/osii/rf-system/rf-coils/solenoid-d205mm-n20-3seg/-/tree/da7e5dd4dc8d00e74652fafee2ac2a91c329667a) |
| [TU Delft GA2](https://gitlab.com/osii/gradient-system/gradient-power-amplifier/tu-delft-15a/tu-delft-ga2) | Gradient amplifier | CERN-OHL-W-2.0 | weak copyleft | No | Yes | Yes | Yes | Gitlab.com | [commit cdb509a2](https://gitlab.com/osii/gradient-system/gradient-power-amplifier/tu-delft-15a/tu-delft-ga2/-/tree/cdb509a2c80e701fabb54fca477cf769921a0934) |
| [Wireless reference implant](https://gitlab1.ptb.de/mri-lab/reference-wireless-implant) | Wireless reference implant hardware | CERN-OHL-W-2.0 | weak copyleft | Yes | No | Yes | Yes | PTB-hosted Gitlab | [commit 055ad252](https://gitlab1.ptb.de/mri-lab/reference-wireless-implant/-/tree/5b5a7079326e74f988b3b535f7e03d7d11c9e0d2) |

<!--- use https://tableconvert.com/ to generate the JSON data from e.g. CSV or clipboard (JSON Format: Array of Object) and then just replace everything _within_ `"items" :`

```json:table

{
    "fields" : [
        {"key": "Project Name", "sortable": true},
        {"key": "Description"},
        {"key": "License", "sortable": true},
        {"key": "License Type", "sortable": true},
        {"key": "Native Files", "sortable": true},
        {"key": "Export Files", "sortable": true},
        {"key": "Bill of Materials", "sortable": true},
        {"key": "Instructions", "sortable": true},
        {"key": "Platform", "sortable": true},
        {"key": "Reference"}
	],
    "items" : [
    {
        "Project Name": "8ch TX/RX RF coil 3T",
        "Description": "low-cost 8-channel transmit/receive 3T head coil for pTx developments, MR safety testing and standardization",
        "License": "CERN-OHL-W-2.0",
        "License Type": "weak copyleft",
        "Native Files": "Yes",
        "Export Files": "Yes",
        "Bill of Materials": "Yes",
        "Instructions": "Yes",
        "Platform": "self-hosted Gitlab",
        "Reference": "commit d02caee3"
    },
    {
        "Project Name": "8ch TX/RX RF coil 7T",
        "Description": "7T 8-channel transmit/receive volume head coil",
        "License": "CERN-OHL-W-2.0",
        "License Type": "weak copyleft",
        "Native Files": "Yes",
        "Export Files": "Yes",
        "Bill of Materials": "No",
        "Instructions": "Yes",
        "Platform": "self-hosted Gitlab",
        "Reference": "commit e6033e92"
    },
    {
        "Project Name": "COSI Measure",
        "Description": "3-axis measurement system",
        "License": "CERN-OHL-1.2",
        "License Type": "strong copyleft",
        "Native Files": "Yes",
        "Export Files": "Yes",
        "Bill of Materials": "Yes",
        "Instructions": "Yes",
        "Platform": "Github.com",
        "Reference": "v1.0"
    },
    {
        "Project Name": "COSI transmit",
        "Description": "RF power amplifier 1kW",
        "License": "CERN-OHL-1.2",
        "License Type": "strong copyleft",
        "Native Files": "Yes",
        "Export Files": "Yes",
        "Bill of Materials": "Yes",
        "Instructions": "Yes",
        "Platform": "Github.com",
        "Reference": "commit fee73e7"
    },
    {
        "Project Name": "GPA-FHDO",
        "Description": "B0 shim or gradient amplifier",
        "License": "CERN-OHL-1.2",
        "License Type": "strong copyleft",
        "Native Files": "Yes",
        "Export Files": "Yes",
        "Bill of Materials": "Yes",
        "Instructions": "Yes",
        "Platform": "Github.com",
        "Reference": "v1.3"
    },
    {
        "Project Name": "GPA-RP-Adapter",
        "Description": "shield for red pitaya to connect it to GPA-FHDO",
        "License": "CERN-OHL-W-2.0",
        "License Type": "weak copyleft",
        "Native Files": "Yes",
        "Export Files": "No",
        "Bill of Materials": "Yes",
        "Instructions": "No",
        "Platform": "Github.com",
        "Reference": "commit a19ebca"
    },
    {
        "Project Name": "H-field RF Probe",
        "Description": "shielded radio frequency magnetic field probe",
        "License": "CERN-OHL-1.2",
        "License Type": "strong copyleft",
        "Native Files": "Yes",
        "Export Files": "Yes",
        "Bill of Materials": "Yes",
        "Instructions": "-",
        "Platform": "Github.com",
        "Reference": "v1.0"
    },
    {
        "Project Name": "High Peak/Average Power TR-Switch for Thermal MR",
        "Description": "high peak/average power transmit/receive switch was developed for thermal MR applications",
        "License": "CERN-OHL-1.2",
        "License Type": "strong copyleft",
        "Native Files": "Yes",
        "Export Files": "Yes",
        "Bill of Materials": "Yes",
        "Instructions": "Yes",
        "Platform": "Github.com",
        "Reference": "v1.0"
    },
    {
        "Project Name": "LimeSDR",
        "Description": "RF prototyping platform",
        "License": "CC-BY-3.0",
        "License Type": "permissive",
        "Native Files": "Yes",
        "Export Files": "Yes",
        "Bill of Materials": "Yes",
        "Instructions": "Yes",
        "Platform": "Github.com",
        "Reference": "1v4"
    },
    {
        "Project Name": "Maker-Gradient-Amplifier",
        "Description": "DIY gradient amplifier",
        "License": "MIT",
        "License Type": "permissive",
        "Native Files": "No",
        "Export Files": "Yes",
        "Bill of Materials": "Yes",
        "Instructions": "No",
        "Platform": "Github.com",
        "Reference": "commit 0f4e5e3"
    },
    {
        "Project Name": "modular multipurpose 32-channel RF signal amplifier and attenuator system",
        "Description": "modular multipurpose 32-channel RF signal amplifier and attenuator system",
        "License": "CERN-OHL-1.2",
        "License Type": "strong copyleft",
        "Native Files": "Yes",
        "Export Files": "Yes",
        "Bill of Materials": "Yes",
        "Instructions": "Yes",
        "Platform": "Github.com",
        "Reference": "commit 2cae1f8"
    },
    {
        "Project Name": "MRI Cage Test Set",
        "Description": "3D-printed antenna set for MRI Faraday RF cages attenuation measurements",
        "License": "CERN-OHL-W-2.0",
        "License Type": "weak copyleft",
        "Native Files": "No",
        "Export Files": "Yes",
        "Bill of Materials": "No",
        "Instructions": "Yes",
        "Platform": "Github.com",
        "Reference": "commit 3452e02"
    },
    {
        "Project Name": "MRI Compatible Wireless Sensors (motion tracking)",
        "Description": "multi-use MRI compatible wireless sensor system using off the shelf components",
        "License": "MIT",
        "License Type": "permissive",
        "Native Files": "Yes",
        "Export Files": "-",
        "Bill of Materials": "Yes",
        "Instructions": "Yes",
        "Platform": "Github.com",
        "Reference": "commit 1dd0414"
    },
    {
        "Project Name": "MRI Distortion phantom",
        "Description": "Modular phantom to measure distortion",
        "License": "CERN-OHL-W-2.0",
        "License Type": "weak copyleft",
        "Native Files": "Yes",
        "Export Files": "No",
        "Bill of Materials": "Yes",
        "Instructions": "Yes",
        "Platform": "Github.com",
        "Reference": "commit 4896369"
    },
    {
        "Project Name": "Open Source NIST Phantom",
        "Description": "",
        "License": "GPL-3.0",
        "License Type": "strong copyleft",
        "Native Files": "No",
        "Export Files": "Yes",
        "Bill of Materials": "Yes",
        "Instructions": "Yes",
        "Platform": "Github.com",
        "Reference": "commit 251e5bd"
    },
    {
        "Project Name": "OpenForce MR",
        "Description": "MR-Compatible force sensor composed of cheap, off-the shelf components",
        "License": "GPL-3.0",
        "License Type": "strong copyleft",
        "Native Files": "Yes",
        "Export Files": "Yes",
        "Bill of Materials": "Yes",
        "Instructions": "Yes",
        "Platform": "Github.com",
        "Reference": "commit d0c5d28"
    },
    {
        "Project Name": "OSI² ONE Magnet",
        "Description": "Halbach MR magnet",
        "License": "CERN-OHL-W-2.0",
        "License Type": "weak copyleft",
        "Native Files": "Yes",
        "Export Files": "Yes",
        "Bill of Materials": "No",
        "Instructions": "No",
        "Platform": "Gitlab.com",
        "Reference": "commit d338fbf0"
    },
    {
        "Project Name": "OSI² ONE RFPA",
        "Description": "RF power amplifier 1kW",
        "License": "CERN-OHL-W-2.0",
        "License Type": "weak copyleft",
        "Native Files": "Yes",
        "Export Files": "Yes",
        "Bill of Materials": "Yes",
        "Instructions": "Yes",
        "Platform": "Gitlab.com",
        "Reference": "commit 1f1f1fd4"
    },
    {
        "Project Name": "Pneumatic Phantom for Mimicking Respiration-Induced MRI Artifacts",
        "Description": "inflation system to mimic the air variation of the human lungs and asses respiration-induced MRI artefacts",
        "License": "MIT",
        "License Type": "permissive",
        "Native Files": "Partially",
        "Export Files": "Partially",
        "Bill of Materials": "No",
        "Instructions": "No",
        "Platform": "OSF.io",
        "Reference": "rev 2022-03-30 18:07 UTC"
    },
    {
        "Project Name": "pTx implant safety testbed",
        "Description": "Parallel transmission hardware and positioning system for all sorts of experiments around RF transmission",
        "License": "CERN-OHL-W-2.0",
        "License Type": "weak copyleft",
        "Native Files": "Yes",
        "Export Files": "-",
        "Bill of Materials": "Yes",
        "Instructions": "Yes",
        "Platform": "self-hosted Gitlab",
        "Reference": "commit 2e66f5db"
    },
    {
        "Project Name": "Solenoid RF coil",
        "Description": "20.5cm diameter",
        "License": "CERN-OHL-W-2.0",
        "License Type": "weak copyleft",
        "Native Files": "Yes",
        "Export Files": "Yes",
        "Bill of Materials": "No",
        "Instructions": "-",
        "Platform": "Gitlab.com",
        "Reference": "commit da7e5dd4"
    },
    {
        "Project Name": "TU Delft GA",
        "Description": "Gradient amplifier +-15A, 12V",
        "License": "CERN-OHL-W-2.0",
        "License Type": "weak copyleft",
        "Native Files": "No",
        "Export Files": "Yes",
        "Bill of Materials": "Yes",
        "Instructions": "Yes",
        "Platform": "Gitlab.com",
        "Reference": "commit cdb509a2"
    },
    {
        "Project Name": "wireless reference implant",
        "Description": "Wireless reference implant hardware for safety testing in MRI",
        "License": "CERN-OHL-W-2.0",
        "License Type": "weak copyleft",
        "Native Files": "Yes",
        "Export Files": "No",
        "Bill of Materials": "Yes",
        "Instructions": "Yes",
        "Platform": "self-hosted Gitlab",
        "Reference": "commit 055ad252"
    }
	],
	"filter" : true,
	"caption" : "open-source hardware for MRI"
}
```
-->

### Pending

- 8ch scalable 19F coil
  - Scalable and Modular independent transmit and receive RF Coil arrays originally developed for 19F cardiac MRI at 3 T
  - no license as by commit `db55480`
- ASL Phantom
  - phantom for perfusion imaging
  - no license as by commit `11d1293`
- EASY_MRI_RFPA
  - BASIC RFPA for MRI applications
  - no license as by `v1.00`
- Current Driver for Local B0-Shim Coils
  - Scalable, multi-channel current supply board that for up to 8 amps per channel
  - no license as by `v7`
- MGH Tabletop MRI
  - low-cost design for educational purpose
  - no license as by `Revision as of 12:59, 23 March 2020 by Jaystock`
- MR Dilution Phantom
  - contrast agent phantom
  - no license as by commit `9fb1e97`
- NMR relaxometry system
  - no license as by commit `7bfa320`
- [OCRA tabletop GPA](https://zeugmatographix.org/ocra/2021/12/15/small-scale-600w-gradient-power-amplifier/)
  - no license as by `Rev003`
- Opencore NMR
  - core modules for implementing an integrated FPGA-based NMR spectrometer
  - no license as by `v2.0.0`
