---
title: OSI² Contribution Guide
...

[TOC]

# Contibution Management

## Markdown Standard

All markdown documents in OSI² shall use [GitLab Flavored Markdown (GLFM)](https://docs.gitlab.com/ee/user/markdown.html),
unless otherwise noted.

## Permissions

- `anyone` can fork and open a merge request
- a `developer` can push code (except to protected branches), review and approve merge requests
- a `maintainer` can do the same + push code to protected branches and alter settings in the repository
- `owner` = almighty admin

## Directory Structure

We're using parts of the Unixish variant of the [(proposed) "OSH project directory structure standards"](https://github.com/hoijui/osh-dir-std/blob/main/mod/unixish/README.md).

Repositories following this base structure are:

- more intuitive to handle (=knowing where to look for or insert files)
- easier to remix (especially when it comes to nesting)
- easier to set up as there's no need to re-invent a directory structure
  (and iterate it over time)

This is the current universal directory template:

```text
.
├── doc (documentation, e.g. assembly guide)
├── res (auxiliary material used/referenced in the repository)
│   ├── img (images)
│   ├── picto (pictograms, symbols, e.g. for graphics)
│   └── spec (data sheets etc.)
└── src (all source files)
```

Of course, sub-folders can be added to this structure and READMEs are allowed on all levels

EXAMPLE: Device made out of 2 PCBs and a casing with connectors

<!--- to create such a tree, just create a local folder structure with dummy files, open the top folder in the terminal and execute:
`tree -F`
-->

```text
./
├── CHANGELOG.md (including design decisions)
├── LICENSE.md (mandatory)
├── README.md (with general project information)
├── doc/
│   ├── assembly-guide.md
│   └── manufacturing-instructions.md
├── res/
│   ├── img/
│   │   └── photos-from-assembly.jpg
│   └── spec/
│       └── data-sheets-of-selected-components.pdf
└── src/
    ├── Casing/
    │   ├── BOM.csv
    │   └── native-design-files.fcstd
    ├── PCB1/
    │   ├── BOM.csv
    │   └── native-design-files.kicad_pcb
    ├── PCB2/
    │   ├── BOM.csv
    │   └── native-design-files.kicad_pcb
    └── README.md (guiding through the subdirectories)
```

## Binaries

Git isn't really good at handling binaries.

**Export/production files should not be in the repository.**
Please use release notes instead (e.g. see the [30cm halbach magnet releases](https://gitlab.com/osii/magnet/30cm-halbach-magnet/-/releases) as a real-world example).

If binaries cannot be avoided, which is usually the case for CAD files, consider using [git LFS](https://git-lfs.com/).
    > find a tutorial [here](https://docs.gitlab.com/topics/git/lfs/)

## Versioning

OSI² uses the [Semantic Versioning Specification v2.0.0](https://semver.org/) definition (where feasible):

Given a version number `MAJOR.MINOR.PATCH`, increment the:

- `MAJOR` version when you make incompatible interface changes
- `MINOR` version when you add new features with backwards compatibility
- `PATCH` version when you make backward compatible bug fixes or other minor changes

**EXAMPLE 1:**

- Console `v1.0.0`
- Console gets a LAN interface now (new feature) → `v1.1.0`
- LAN interface changes to USB-C (=incompatible interface change) → `v2.0.0` 

**EXAMPLE 2:**

- 30cm Halbach Magnet `v1.0.0`
- new simulation of magnetic field → improved orientations of magnet array → `v2.0.0`
- improvement of rod positioning → `v2.1.0`
- bug fix: additional holes in lid for screw heads → `v2.1.1`

Assemblies made from independently versioned submodules behave accordingly.
However, to reflect the complexity of the system, the rules are a bit more ambiguous:

- `MAJOR` version when the system changes significantly
- `MINOR` version when at least one module changes significantly (e.g. new major version)
- `PATCH` version when for minor changes and bug fixes

- OSI² ONE `v1.0.0` with Console `v1.0.0`, 30cm Halbach Magnet `v1.0.0`
- update to Console `v1.1.0` (new LAN interface, not deemed significant for the system) → OSI² ONE `v1.0.1`
- update to Magnet `v2.0.0` (new magnet array, stronger field, shorter magnet) → OSI² ONE `v1.1.1`
- most modules change to new major versions → OSI² ONE `v2.0.0`

When changes are made that affect the interface of OSI² ONE with the outer world significantly (e.g. much smaller magnet diameter) it's most likely that the _scope_ of that assembly is also a different one now, hence a different model name and repository should be used (e.g. OSI² Mini)

## Branches

```mermaid
%%{init: {
    "theme": "default",
    "logLevel": "debug",
    "themeVariables": {
        "git0": "#608355",
        "git1": "#a0db8e",
        "git2": "#bce5af",
        "git3": "#a4b3e2",
        "git4": "#ffdf32",
        "git5": "#e2a4b3"
    }
} }%%
gitGraph
    branch rc/1
    checkout rc/1
    branch rc/1.0
    branch releases/1.0
    branch FORK_mdr/1.0
    branch releases/1.0-MDR
```

| branch        | for…                                                              |
|---------------|-------------------------------------------------------------------|
| `main`        | development of `MAJOR` versions                                   |
| `rc/x`        | development of `MINOR` versions                                   |
| `rc/x.y`      | development of `PATCH` versions                                   |
| `releases/x.y` | releases of the same `MINOR` version (e.g. `v1.0.0` and `v1.0.1`) |
| `releases/x.y-MDR` | MDR releases of the same `MINOR` version (e.g. `v1.0.2-MDR`)      |

- `rc` = **R**elease **C**andidate
- `release branches` are **protected** (meaning that only `maintainers` and `owners` can push)
- work on `MDR releases` happens in dedicated forks (for cleaner issue management)
- **never** merge from `release branches` to `main`! (otherwise tags will get very messy very fast)

## Tagging

- `release tags` on `release branches`
- tags on main for better orientation:
  - `<latest-release>.increment`
  - example:
    - `1.1.0.1` on `main` after `1.1.0` has been released on `release/1.0`
    - `1.1.0.2` on `main` after patch `1.0.3` has been released right after

## MDR Variants

Specific versions of developed hard- and software will get documentation
according to [Regulation (EU) 2017/745 for Medical Devices (MDR)](
https://eur-lex.europa.eu/eli/reg/2017/745/oj).
These MDR variants are essentially the selected release + MDR documentation,
so both versions are, form the technical standpoint, identical.\
However, e.g. `console v1.1.3` and `console v1.1.3-MDR` are published on different branches, because:

1. MDR is a quite lengthy process
   (which would block further `PATCH` releases for `console v1.1.3`) and
2. not every release gets MDR documentation (because of 1.).

As a graphic:

![MDR Variants](images/mdr-variant.svg){width=60%}

Comments:

- `OSI² ONE.1.4` is the repository for the actual scanner (the assembly)
  and refers to all module releases it consists of.
- MDR variants are made out of the "bare tech" variants.
- There's no order defined for MDR development,
  however it might be wise to start with the modules and do the scanner later.

## Development Scenarios

### Repository Setup

- draft a `README`
- add a [`LICENSE`](License-Recommendation.md)
- open first major rc branch: `rc/1`

### Publish a `MINOR` Version

```mermaid
%%{init: {
    "theme": "default",
    "logLevel": "debug",
    "themeVariables": {
        "git0": "#608355",
        "git1": "#a0db8e",
        "git2": "#bce5af",
        "git3": "#a4b3e2",
        "git4": "#ffdf32",
        "git5": "#e2a4b3"
    }
} }%%
gitGraph
    commit
    branch rc/1
    checkout rc/1
    commit
    commit
    commit
    branch rc/1.0
    commit
    branch releases/1.0
    commit tag:"1.0.0"
    checkout rc/1
    merge rc/1.0
    checkout main
    merge rc/1 tag:"1.0.0.1"
```

1. go to `rc/1`
2. work until you're happy with the result
3. **Design Freeze:**
	- move current version of `rc/1` → `rc/1.0` (create branch from last commit)
    - only bug fixes from here on
    - development of new features can of course continue on `rc/1`
4. minor changes & bug fixes
5. **publish release `1.0.0`:**
	- move current version of `rc/1.0` → `releases/1.0` (create branch from last commit)
	- add the tag `1.0.0` on `releases/1.0`
6. merge changes back to the roots:
	- merge current version of `rc/1.0` → `rc/1`
	- merge current version of `rc/1` → `main`
	- add tag `1.0.0.1` on main

**We highly recommend to perform merges locally!**

The online interface of GitLab.com (as well as many git clients)
isn't very good at handling binary files, e.g. CAD models.
For details on merging and using git,
please see the [FAQ](#faq) at the bottom of this document.

### Publish a `PATCH` Version

```mermaid
%%{init: {
    "theme": "default",
    "logLevel": "debug",
    "themeVariables": {
        "git0": "#608355",
        "git1": "#a0db8e",
        "git2": "#bce5af",
        "git3": "#a4b3e2",
        "git4": "#ffdf32",
        "git5": "#e2a4b3"
    }
} }%%
gitGraph
    commit
    branch rc/1
    checkout rc/1
    commit
    commit
    commit
    branch rc/1.0
    commit
    branch releases/1.0
    commit tag:"1.0.0"
    checkout rc/1
    merge rc/1.0
    checkout main
    merge rc/1 tag:"1.0.0.1"
    checkout rc/1.0
    commit
    checkout releases/1.0
    merge rc/1.0 tag:"1.0.1"
    checkout rc/1
    merge rc/1.0
    checkout main
    merge rc/1 tag:"1.0.1.1"
```

1. go to `rc/1.0`
2. fix bugs
3. publish release `1.0.1`:
	- merge current version of `rc/1.0` → `releases/1.0`
	- add the tag `1.0.1` on `releases/1.0`
4. merge changes back to the roots:
	- merge current version of `rc/1.0` → `rc/1`
	- merge current version of `rc/1` → `main`
	- add tag `1.0.1.1` on main

You can repeat this as many times as you like at any point of time
without interference with any other ongoing developments.

### Publish MDR Version

```mermaid
%%{init: {
    "theme": "default",
    "logLevel": "debug",
    "themeVariables": {
        "git0": "#608355",
        "git1": "#a0db8e",
        "git2": "#bce5af",
        "git3": "#a4b3e2",
        "git4": "#ffdf32",
        "git5": "#e2a4b3"
    }
} }%%
gitGraph
    commit
    branch rc/1
    checkout rc/1
    commit
    commit
    commit
    branch rc/1.0
    commit
    branch releases/1.0
    commit tag:"1.0.0"
    checkout rc/1
    merge rc/1.0
    checkout main
    merge rc/1 tag:"1.0.0.1"
    checkout rc/1.0
    commit
    checkout releases/1.0
    merge rc/1.0 tag:"1.0.1"
    checkout rc/1
    merge rc/1.0
    checkout main
    merge rc/1 tag:"1.0.1.1"
    checkout releases/1.0
    branch FORK_mdr/1.0
    commit
    checkout rc/1.0
    commit
    checkout releases/1.0
    merge rc/1.0 tag:"1.0.2"
    checkout rc/1
    merge rc/1.0
    checkout main
    merge rc/1 tag:"1.0.2.1"
    checkout FORK_mdr/1.0
    merge releases/1.0
    commit
    branch releases/1.0-mdr
    commit tag:"1.0.2-mdr"
```

Given that we want to create MDR documentation for release `1.0.1`:

1. go to tag `1.0.1` on `releases/1.0`
2. fork it into a dedicated repository under the group `OSI² QM` (e.g. "`mdr/1.0`")
3. work in this fork
4. if you need to apply smaller changes or fix bugs for MDR-compliance,
   create a new patch version in the main repository and update the fork from there:
	- go to `rc/1.0`
	- fix the bug (following the [instructions above](#publish-a-patch-version)), creating `1.0.2`
	- update your MDR fork ("`FORK_mdr/1.0`" in the diagram)
5. work until MDR-compliance is officially approved
6. merge fork into a new, dedicated branch: `releases/1.0-mdr`
7. and add the tag `1.0.2-mdr`

<!--- full graph to fix: 
https://mermaid.live/edit#pako:eNq1Vm1r2zAQ_itCI7BBmlp2nSb-XDbGVgYd7MMwDMU-O6K2FGS5Lwv575PfUjlWHG8wB4Kt55670yOddHsciRhwgGezPeNMBWgfcqSfEKst5BDiQL_GkNAyUyGed2Am0q_wBFmHb8rUQGvqDyoZ3WRQVDat1wZOmXIa4ruls_J8_0g94qTFqRNvVjDE3Q6_2XjgDnGvxTcR-DQZ4jctniRx4ln4_gX-8kL82xYHt7IIcQMfQn5Ah9ks5Nrkk6S7bTMeiTxnCn2-C0JcrQKjWTvWMU0LCTuBdvqfSohRIiR6Ip3dRlIebZGMrklL3EL0KEplDhm-noV8RIJrDwtn4dij0fi1jWLadJEgA1pAca3BITlhslCdTYiRomklSz9Yl2FOGW-GcpAp1Bk3btrvxg1SojHtu1uQgcO3KRuyHNOcLIzdsW26ugpQwl4sar1RB3q9TXbhmDMam4_JmaKi6dbmeJBUK9jHbw9ffuWxtMSxQBb57u8eJquXM651i7aUp1DUGlrZkwV0_4-ArkXAoRi9LXuuOkqpT0qJLok1ZHJ41t8iQwlQVUqwFW4hckAblqY6Qrsxq7N4WBDkXEGT89L3c6rUqcQhE6t6QUyKoacpTjOzppZcmwXlopbvjOW4gnF1e4ldn0zO7Jkztd4rdRe9f2ZqW1H0On74l30b4tqTZ8-BXMqBjB83ZHQVJp425G9Xl0yqlv6dwVmxNYvBctVcVfB5gQ14cBtp6ORGauv6anr9RYIrxktAxjVRbx08ry4rrUqse6q66RlppMbaqJEmqtdArZxE_4zey2ieHIdS5wRzj43PUj99zDvykuSU1zVMtH76mD_CW47Euz3Js4KqFkmLSEslvr_yCAcJzQqY43IXUwV3jKaS5sdRiJkS8r5pYetOdo53lP8UQtsoWUL9iYM9fsHBar1w1mt_5d0Q3_X0yxy_4oC4_sIlxHU9Xw-vibM6zPHv2oHeu7eeu17eaGS5Wjqef_gDwC1P4w
-->

## FAQ

### git Clients

**_I have zero experience with git and don't want to work in the terminal._**

The git logic you'll have to learn in any case, if you want to participate in the development here.
However, you won't have to memorize all the git commands and work from the terminal, if you don't want to.

- One of the simplest graphical interfaces available is [ungit](https://github.com/FredrikNoren/ungit/).
  It runs in your web browser and lets you perform all steps graphically.
- [VSCode](https://code.visualstudio.com/) / [VSCodium](https://vscodium.com/)
  come with a preinstalled git client that's fairly easy to use.
- If you want to be close to the command line,
  without actually typing commands,
  [lazygit](https://github.com/jesseduffield/lazygit) features a powerful and easy-to-use tool for you.
  It's fast and direct and surprisingly intuitive.
  If you're lost, just hit `?` (yes, using the shift key)
  and the tooltip will show you the available options
  for whichever sub-menue you're currently navigating in.

### Merging

**_I ran into merge conflicts, how can I resolve them?_**

First of all, as indicated above, _we highly recommend to perform merges locally!_
The online interface of GitLab.com (as well as many git clients)
isn't very good at handling binary files, e.g. CAD models.

Performing a merge locally can be done with the steps below
(given that you have all permissions and your SSH is set up correctly):

Given that you want to merge from `rc/2` to `main`,
from the repository folder execute:

```shell
git checkout main
git merge rc/2
```

git will now try to pull all changes from `rc/2` and perform the merge automatically.
If it encounters conflicts, it will point them out to you.
From here you can either proceed with the command-line
or use a graphical merge tool,
guiding you through the conflicts.
We recommend [Meld](https://meldmerge.org/) as a merge tool,
but there're lots of alternatives, as well.

Call the merge tool with:

```shell
git mergetool
```

And go through all the conflicts.
The merge tool will ask you which changes to keep in conflicting files,
or – in the case of binary files that cannot be compared – which file to keep.

Once you're done, finalize the merge with:

```shell
git merge --continue
git commit -m "YOUR COMMIT MESSAGE"
git push
```
