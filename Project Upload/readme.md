## How to upload projects to Opensourceimaging.org

Please provide the following information for an open-source project upload to Opensourceimaging.org:

- Title (short title of the project)
- Application (short description, max 30 words)
- Contributors (please also include affiliations of each contributor)
- Affiliations (affiliations of the contributors)
- Contact (email address of a contact person)
- Estimated cost (estimated hardware cost, in case of software please type free)
- Progress (Please choose: under development (early stage project, no prototype/alpha version or documentation ready yet), prototype/alpha- or beta-version (first version ready, documentation work in progress) or stable release (please provide the latest release version and the open source license used))
- Resources (links to the project website and/or wiki, developer sites, repositories etc)
- Abstract (abstract/specifications of the open source soft/-hardware. The abstract should give a good overview of the functionality of the tool)
- Publications (any publications, conference abstracts published about the soft-/hardware)
- Figures: We need a featured image and ideally 3 figures or more to highlight the project, (Add a short caption (<100 characters) to each figure. If possible the format of the figures should be: featured image: PNG with transparent background, 500x500pixels
additional images: JPG, 60% compression, not larger than 1600x850 pixels. If you use the word/libreOffice template please don’t insert the figures into document file, you can make a commit to the project folder with your files.)


## How to change project information on Opensourceimaging.org

If you would like to change any information in projects on opensourceimaging.org or in this repository, please create an issue in this repository, commit the changes or contact info@opensourceimaging.org. 